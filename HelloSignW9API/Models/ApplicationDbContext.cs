﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloSignW9API.Models
{
    public class ApplicationDbContext :     DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<Winner> Winners { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
