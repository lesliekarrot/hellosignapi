﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HelloSignW9API.Models
{
    public class Winner
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [EmailAddress]
        [RegularExpression("^[a-z0-9_\\+-]+(\\.[a-z0-9_\\+-]+)*@[a-z0-9-]+(\\.[a-z0-9]+)*\\.([a-z]{2,4})$", ErrorMessage = "Invalid email format.")]
        public string Email { get; set; }       
        public string Date { get; set; }      

        [Required]
        public string Name { get; set; }
        public string Adress { get; set; }
        public string City { get; set; }
        public string TaxClassification { get; set; }
        public string AccountNumber { get; set; }
        public string RequestersName { get; set; }
        public string SocialSecurityNumber { get; set; }
        public string EmployerNumber { get; set; }
        public string Signature { get; set; }
        public byte[] FormW9 { get; set; }
        public string SignId { get; set; }
    }
}
