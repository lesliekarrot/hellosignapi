﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace HelloSignW9API.Models
{
    public class User
    {

     
        [Key]
        public int Id { get; set; }

             
        [Required(ErrorMessage = "Please enter your Email.")]
        [Display(Name = "Username : ")]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please enter your Password.")]
        [Display(Name = "Password : ")]
        public string Password { get; set; }



        //Login credentials
        public String LoginProcess(String strUsername, String strPassword)
        {
            String message = "";
              

            SqlConnection con = new SqlConnection("Data Source=LAPTOP-DPNR5R00;Initial Catalog=HelloSignW9API;Integrated Security=True");
            SqlCommand cmd = new SqlCommand("Select * from Users where Email=@Username", con);
            cmd.Parameters.AddWithValue("@Username", strUsername);
            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    Boolean login = (strPassword.Equals(reader["Password"].ToString(), StringComparison.InvariantCulture)) ? true : false;
                    if (login)
                    {
                        message = "1";
           

                    }
                    else
                        message = "Invalid Credentials";
                }
                else
                    message = "Invalid Credentials";

                reader.Close();
                reader.Dispose();
                cmd.Dispose();
                con.Close();
            }
            catch (Exception ex)
            {
                message = ex.Message.ToString() + "Error.";

            }
            return message;
        }
    }
}
