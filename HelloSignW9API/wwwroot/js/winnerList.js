﻿
var dataTable;

$(document).ready(function () {
    loadDataTable();
});

function loadDataTable() {
    dataTable = $('#DT_load').DataTable({
        "ajax": {
            "url": "/winners/getall/",
            "type": "GET",
            "datatype": "json"
        },
        "columns": [
            { "data": "name", "width": "20%" },
            { "data": "email", "width": "20%" },        
             
            {
                "data": "id",
                "render": function (data) {
                    return `<div class="text-center">
                        <a href="/Winners/Update?id=${data}" class='btn btn-info text-white' style='cursor:pointer; width:60px;'>
                           Info
                        </a>                       
                        </div>`;
                }, "width": "20%"
            }
        ],
        "language": {
            "emptyTable": "no data found"
        },
        "width": "100%"
    });
}