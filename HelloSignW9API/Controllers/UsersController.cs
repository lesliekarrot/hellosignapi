﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HelloSignW9API.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace HelloSignW9API.Controllers
{
    public class UsersController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }


        //Login method
        [HttpPost]
        public ActionResult Login(User users)
        {
            if (ModelState.IsValid)
            {
               
                String message = users.LoginProcess(users.Email, users.Password);
                //RedirectToAction("actionName/ViewName_ActionResultMethodName", "ControllerName");
                if (message.Equals("1"))
                {
                   
                    return RedirectToAction("Index", "Winners");
                }
                else
                    ViewBag.ErrorMessage = message;
            }
            return View(users);
        }
    }
}