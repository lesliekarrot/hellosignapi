﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HelloSign;
using HelloSignW9API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HelloSignW9API.Controllers
{
    public class WinnersController : Controller
    {
        private readonly ApplicationDbContext _db;
        [BindProperty]
        public Winner Winner { get; set; }

        public WinnersController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
          
            return View();
        }

        //CREATE
        public IActionResult Create(int? id)
        {
            Winner = new Winner();
            if (id == null)
            {
                return View(Winner);
            }                    


            return View(Winner);

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create()
        {
           
            if (ModelState.IsValid)
            {
                
                if(Winner.Id == 0)
                {
                    Winner.Date = Convert.ToString(DateTime.Now);


                    var client = new Client("c630c721fc54967c46d087051cb723df25eb3345d1fa53a1989b5614345bf6b5");
                    var request = new TemplateSignatureRequest();

                    request.AddTemplate("6ffae7883ad011c21ea8adced0c002f7df5ea428");
                    request.Subject = "Testing template.";
                    request.Message = "Testing template.";
                    request.AddCustomField("DateSigned", Winner.Date);
                    request.AddSigner("Client", Winner.Email, Winner.Name);
                    request.SigningOptions = new SigningOptions
                    {
                        Draw = true,
                        Type = true,
                        Upload = true,
                        Phone = false,
                        Default = "draw"
                    };
                    request.TestMode = true;
                    var response = client.SendSignatureRequest(request);
                    Winner.SignId = response.SignatureRequestId;
                    Console.WriteLine("New Template Signature Request ID: " + response.SignatureRequestId);

                    _db.Winners.Add(Winner);
                }
                _db.SaveChanges();                            

              
                return RedirectToAction("Index");
            }

            return View(Winner);
        }

        //UPDATE
        public IActionResult Update(int? id, string fileName)
        {
            Winner = new Winner();
            Winner = _db.Winners.FirstOrDefault(u => u.Id == id);           


            if (Winner == null)
            {
                return NotFound();
            }

            return View(Winner);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update()
        {            
            if (ModelState.IsValid)
            {
                if (Winner.Id == 0)
                {
                   
                } else
                {
                    string signatureId = Winner.SignId;
                    var client = new Client("c630c721fc54967c46d087051cb723df25eb3345d1fa53a1989b5614345bf6b5");
                    client.DownloadSignatureRequestFiles(signatureId, "C:/Users/Leslie Zaragosa/Downloads/output.pdf");

                    //STATUS

                    var request = client.GetSignatureRequest(signatureId);
                    var status = request.Signatures;

                    Console.WriteLine("Signature Request title: " + status);

                    _db.Winners.Update(Winner);
                }
                _db.SaveChanges();
              //  string path = Path.Combine(@"C:\Users\Leslie Zaragosa\Downloads", "output.pdf");
              //  return File(path, "application/pdf");
              //  return RedirectToAction("Index");
            }

            return View(Winner);
        }

        #region

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Json(new { data = await _db.Winners.ToListAsync() });
        }

        #endregion
    }
}