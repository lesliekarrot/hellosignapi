﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HelloSignW9API.Migrations
{
    public partial class AddColumnSignID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RememberMe",
                table: "Users");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Winners",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SignId",
                table: "Winners",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SignId",
                table: "Winners");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Winners",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<bool>(
                name: "RememberMe",
                table: "Users",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
